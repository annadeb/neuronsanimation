﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NeuronsAnimation
{
    /// <summary>
    /// Logika interakcji dla klasy Neuron.xaml
    /// </summary>
    public partial class Neuron : Window
    {
        public Neuron()
        {
            InitializeComponent();
        }

        
        private void BImpuls1_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Teraz zobaczysz animację szerzenia się impulsu nerwowego \n w komórce.", "Informacja!", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void BImpuls2_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Teraz zobaczysz animację szerzenia się impulsu nerwowego \n w komórce.", "Informacja!", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void BImpuls3_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Teraz zobaczysz animację szerzenia się impulsu nerwowego \n w komórce.", "Informacja!", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void BImpuls4_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Teraz zobaczysz animację szerzenia się impulsu nerwowego \n w komórce.", "Informacja!", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void BImpuls5_Click(object sender, RoutedEventArgs e)
        {
           
        }
    }
}
